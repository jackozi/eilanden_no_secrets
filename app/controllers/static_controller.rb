class StaticController < ApplicationController
  def home
    #enable offers
    @offer = Section.find(2).active_sections.first
    @offers = Offer.all if @offer
    #enable slider
    @slider = Section.find(1).active_sections.first
    @sliders = MainSlider.all if @slider
    #enable gallery
    @gallery = Section.find(3).active_sections.first
    @images = Image.all if @gallery
    #enable rooms
    @rooms = Section.find(4).active_sections.first
    @roomlist = Room.all if @rooms
    #enable info
    @info = Section.find(5).active_sections.first
    @weather = ForecastIO.forecast(37.8267, -122.423, params: { units: 'si' })
  end
end
