class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_paper_trail_whodunnit
  layout :layout

  private

  def layout
    is_a?(Devise::SessionsController) ? false : "application"
  end
end
