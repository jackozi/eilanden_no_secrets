class ActiveSection < ApplicationRecord
  has_paper_trail
  belongs_to :section
end
