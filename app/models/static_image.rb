class StaticImage < ApplicationRecord
  has_paper_trail
  has_attached_file :asset,
    :styles => {
    :thumb => "100x300"
  }
    validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/
end
