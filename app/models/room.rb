class Room < ApplicationRecord
  has_attached_file :asset,
    :styles => {
      :thumb => "100x200" 
  }
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/
end
