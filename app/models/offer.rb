class Offer < ApplicationRecord
  has_attached_file :asset,
    :styles => {
      :small => "150x150>"
  }
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/
  has_many :features
  has_many :options, through: :features
end
