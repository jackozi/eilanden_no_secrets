class Section < ApplicationRecord
  has_paper_trail
  has_many :active_sections
end
