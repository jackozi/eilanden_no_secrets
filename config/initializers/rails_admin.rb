RailsAdmin.config do |config|
  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end

  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration
  config.excluded_models << "Section" << "Feature"

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      except ["Setting", "StaticImage", "SocialMedium"]
    end

    export
    bulk_delete do
      except ["Setting", "StaticImage", "SocialMedium"]
    end

    show
    edit
    delete do
      except ["Setting", "StaticImage", "SocialMedium"]
    end

    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  config.model ActiveSection do
    field :section do
      inline_add false
      inline_edit false
    end

    field :title
    field :subtitle
  end

  config.model MainSlider do
    list do 
      exclude_fields :created_at, :updated_at
    end
  end
  
  config.model Image do 
    list do 
      exclude_fields :created_at, :updated_at
    end
  end

  config.model SocialMedium do
    field :name do
      read_only true
    end
    field :url
  end

  config.model StaticImage do 
    list do 
      exclude_fields :id, :created_at, :updated_at
    end
    field :name do 
      read_only true
    end
    field :asset
  end

  config.model Offer do 
    list do
      exclude_fields :created_at, :updated_at
    end
  end

  config.model Option do 
    list do
      exclude_fields :created_at, :updated_at
    end
  end

  config.model Room do 
    list do 
      exclude_fields :created_at, :updated_at
    end
  end

  config.model Setting do
    field :name do
      read_only true
    end

    field :value
  end
end
