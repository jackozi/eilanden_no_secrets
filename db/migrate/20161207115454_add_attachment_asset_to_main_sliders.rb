class AddAttachmentAssetToMainSliders < ActiveRecord::Migration
  def self.up
    change_table :main_sliders do |t|
      t.attachment :asset
    end
  end

  def self.down
    remove_attachment :main_sliders, :asset
  end
end
