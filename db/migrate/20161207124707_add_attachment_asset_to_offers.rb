class AddAttachmentAssetToOffers < ActiveRecord::Migration
  def self.up
    change_table :offers do |t|
      t.attachment :asset
    end
  end

  def self.down
    remove_attachment :offers, :asset
  end
end
