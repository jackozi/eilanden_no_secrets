class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.integer :order
      t.string :title
      t.string :subtitle
      t.string :price
      t.string :type

      t.timestamps
    end
  end
end
