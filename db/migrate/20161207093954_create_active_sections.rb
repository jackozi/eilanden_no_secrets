class CreateActiveSections < ActiveRecord::Migration[5.0]
  def change
    create_table :active_sections do |t|
      t.references :section, foreign_key: true, required: true
      t.string :title, required: true
      t.string :subtitle, required: true

      t.timestamps
    end
  end
end
