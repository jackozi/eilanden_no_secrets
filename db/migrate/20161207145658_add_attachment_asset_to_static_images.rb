class AddAttachmentAssetToStaticImages < ActiveRecord::Migration
  def self.up
    change_table :static_images do |t|
      t.attachment :asset
    end
  end

  def self.down
    remove_attachment :static_images, :asset
  end
end
