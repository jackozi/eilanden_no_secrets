# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
USED_SECTIONS = [
  #DO NOT CHANGE ORDER OR KITTENS WILL DIE!!
  "Sliders",
  "Offers",
  "Gallery", 
  "Rooms",
  "Info"
]

USED_SECTIONS.each do |us|
  Section.create(name: us)
end

#Create social media buttons
SocialMedium.create(name: "Twitter", url: "www.twitter.com")
SocialMedium.create(name: "Facebook", url: "www.facebook.com")
SocialMedium.create(name: "Google", url: "plus.google.com")
#Create first user to setup app with
User.create(email: "demo@eilanden.nl", password: "changemeasap")
#Create settings in db
Setting.create(name: "title", value: "Eilanden Title")
Setting.create(name: "name", value: "Boutique Hotel De Eilanden")
Setting.create(name: "subtitle", value: "Zorgeloos overnachten")
Setting.create(name: "Main info", value: "Hier een verhaal")
Setting.create(name: "Footer text", value: "Hier een ander verhaal")
Setting.create(name: "Address", value: "Harlingen, Nederland")
Setting.create(name: "Phone", value: "1234567890")
Setting.create(name: "email", value: "info@hoteldeeilanden.nl")
Setting.create(name: "copyright", value: "Eilanden")
Setting.create(name: "Book button", value: "Reserveer nu")
Setting.create(name: "Bookingbar Title", value: "Vind een kamer")
Setting.create(name: "Bookingbar Subtitle", value: "Die bij u past")
#Create static image fields
StaticImage.create(name: "Main info")
